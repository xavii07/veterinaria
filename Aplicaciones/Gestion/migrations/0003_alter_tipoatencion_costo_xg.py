# Generated by Django 5.0.1 on 2024-01-04 06:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Gestion', '0002_medicamento'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tipoatencion',
            name='costo_xg',
            field=models.DecimalField(decimal_places=2, max_digits=5),
        ),
    ]
