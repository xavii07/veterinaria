from django.db import models

# Create your models here.

class Pais(models.Model):
    id_xg = models.AutoField(primary_key=True)
    nombre_xg = models.CharField(max_length=50)
    codigo_ISO_xg = models.CharField(max_length=50)
    capital_xg = models.CharField(max_length=50)
    idioma_xg = models.CharField(max_length=50)
    continente_xg = models.CharField(max_length=50)
    bandera_xg = models.FileField(upload_to='paises', null=True, blank=True)

    def __str__(self):
        return self.nombre_xg

class EstadoCivil(models.Model):
    id_xg = models.AutoField(primary_key=True)
    nombre_xg = models.CharField(max_length=50)
    descripcion_xg = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre_xg

class Propietario(models.Model):
    id_xg = models.AutoField(primary_key=True)
    nombres_xg = models.CharField(max_length = 50)
    apellidos_xg = models.CharField(max_length = 50)
    direccion_xg = models.CharField(max_length = 50)
    telefono_xg = models.CharField(max_length = 10)
    pais_xg = models.ForeignKey(Pais, on_delete=models.CASCADE, null=True)
    estado_civil_xg = models.ForeignKey(EstadoCivil, on_delete=models.CASCADE, null=True)
    copia_cedula_xg = models.FileField(upload_to='propietarios/cedulas', null=True, blank=True)

    def __str__(self):
        return self.nombres_xg

class Raza(models.Model):
    id_xg = models.AutoField(primary_key=True)
    nombre_xg = models.CharField(max_length=50)
    descripcion_xg = models.CharField(max_length=100)
    tamanio_xg = models.CharField(max_length=50)
    esperanza_vida_xg = models.CharField(max_length=50)
    origen_xg = models.CharField(max_length=50)

    def __str__(self):
        return self.nombre_xg

class Mascota(models.Model):
    id_xg = models.AutoField(primary_key=True)
    fecha_nacimiento_xg = models.DateField()
    color_xg = models.CharField(max_length=50)
    peso_xg = models.DecimalField(max_digits=5, decimal_places=2)
    nombre_xg = models.CharField(max_length=50)
    propietario_xg = models.ForeignKey(Propietario, on_delete=models.CASCADE, null=True)
    raza_xg = models.ForeignKey(Raza, on_delete=models.CASCADE, null=True)
    fotografia_xg = models.FileField(upload_to='mascotas', null=True, blank=True)

    def __str__(self):
        return self.nombre_xg


class TipoAtencion(models.Model):
    id_xg = models.AutoField(primary_key=True)
    nombre_xg = models.CharField(max_length=50)
    descripcion_xg = models.CharField(max_length=100)
    duracion_xg = models.CharField(max_length=50)
    costo_xg = models.DecimalField(max_digits=5, decimal_places=2)
    fecha_creacion_xg = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.nombre_xg

class Atencion(models.Model):
    id_xg = models.AutoField(primary_key=True)
    fecha_xg = models.DateField()
    descripcion_xg = models.CharField(max_length=100)
    seguimiento_xg = models.CharField(max_length=250)
    tipo_atencion_xg = models.ForeignKey(TipoAtencion, on_delete=models.CASCADE, null=True)
    mascota_xg = models.ForeignKey(Mascota, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.descripcion_xg

class Medicamento(models.Model):
    id_xg = models.AutoField(primary_key=True)
    nombre_xg = models.CharField(max_length=50)
    fecha_vencimiento_xg = models.DateField()
    stock_xg = models.IntegerField()
    costo_xg = models.DecimalField(max_digits=5, decimal_places=2)
    proveedor_xg = models.CharField(max_length=50)
    fotografia_xg = models.FileField(upload_to='medicamentos', null=True, blank=True)

    def __str__(self):
        return self.nombre_xg

class Receta(models.Model):
    id_xg = models.AutoField(primary_key=True)
    fecha_xg = models.DateField()
    instrucciones_xg = models.CharField(max_length=250)
    notas_adicionales_xg = models.CharField(max_length=250)
    atencion_xg = models.ForeignKey(Atencion, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.instrucciones_xg

class Detalle (models.Model):
    id_xg = models.AutoField(primary_key=True)
    cantidad_xg = models.IntegerField()
    dosis_xg = models.IntegerField()
    frecuencia_xg = models.IntegerField()
    medicamento_xg = models.ForeignKey(Medicamento, on_delete=models.CASCADE, null=True)
    receta_xg = models.ForeignKey(Receta, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.frecuencia_xg