from django.shortcuts import render, redirect
from .models import Raza, Pais, EstadoCivil, TipoAtencion, Medicamento, Propietario, Mascota, Atencion, Receta, Detalle
from django.contrib import messages

#TODO: Raza
def listadoRazas(request):
    razasDbb = Raza.objects.all()
    return render(request, 'razas.html', {
        "razas": list(enumerate(razasDbb))
    })

def crearRaza(request):
    nombre = request.POST['nombre_xg']
    descripcion = request.POST['descripcion_xg']
    tamanio = request.POST['tamanio_xg']
    esperanza_vida = request.POST['esperanza_vida_xg']
    origen = request.POST['origen_xg']

    newRaza = Raza.objects.create(nombre_xg=nombre, descripcion_xg=descripcion, tamanio_xg=tamanio, esperanza_vida_xg=esperanza_vida, origen_xg=origen)
    messages.success(request, 'Raza creada correctamente')

    return redirect('/')

def eliminarRaza(request, id):
    razaDb = Raza.objects.get(id_xg=id)
    razaDb.delete()
    messages.success(request, 'Raza eliminada correctamente')
    return redirect('/')

def editarRazaView(request, id):
    razaDb = Raza.objects.get(id_xg=id)
    return render(request, 'editarRaza.html', {'raza': razaDb})

def actualizarRaza(request):
    id=request.POST['id_xg']
    nombre = request.POST['nombre_xg']
    descripcion = request.POST['descripcion_xg']
    tamanio = request.POST['tamanio_xg']
    esperanza_vida = request.POST['esperanza_vida_xg']
    origen = request.POST['origen_xg']

    razaEditar = Raza.objects.get(id_xg=id)

    razaEditar.nombre_xg = nombre
    razaEditar.descripcion_xg = descripcion
    razaEditar.tamanio_xg = tamanio
    razaEditar.esperanza_vida_xg = esperanza_vida
    razaEditar.origen_xg = origen

    razaEditar.save()
    messages.success(request, 'Raza actualizado exitosamente')
    return redirect('/')

#TODO: Pais
def listadoPaises(request):
    paisesDbb = Pais.objects.all()
    return render(request, 'paises.html', {
        "paises": list(enumerate(paisesDbb))
    })

def crearPais(request):
    nombre = request.POST['nombre_xg']
    codigo_ISO = request.POST['codigo_ISO_xg']
    capital = request.POST['capital_xg']
    idioma = request.POST['idioma_xg']
    continente = request.POST['continente_xg']
    bandera = request.FILES.get('bandera_xg')

    newPais = Pais.objects.create(nombre_xg=nombre, codigo_ISO_xg=codigo_ISO, capital_xg=capital, idioma_xg=idioma, continente_xg=continente, bandera_xg=bandera)
    messages.success(request, 'Pais creado correctamente')

    return redirect('/paises')

def eliminarPais(request, id):
    paisDb = Pais.objects.get(id_xg=id)
    paisDb.delete()
    messages.success(request, 'Pais eliminado correctamente')
    return redirect('/paises')

def editarPaisView(request, id):
    paisDb = Pais.objects.get(id_xg=id)
    return render(request, 'editarPais.html', {'pais': paisDb})

def actualizarPais(request):
    id=request.POST['id_xg']
    nombre = request.POST['nombre_xg']
    codigo_ISO = request.POST['codigo_ISO_xg']
    capital = request.POST['capital_xg']
    idioma = request.POST['idioma_xg']
    continente = request.POST['continente_xg']

    paisEditar = Pais.objects.get(id_xg=id)
    paisEditar.nombre_xg = nombre
    paisEditar.codigo_ISO_xg = codigo_ISO
    paisEditar.capital_xg = capital
    paisEditar.idioma_xg = idioma
    paisEditar.continente_xg = continente

    paisEditar.save()
    messages.success(request, 'Pais actualizado exitosamente')
    return redirect('/paises')

#TODO: EstadoCivil
def listadoEstadosCiviles(request):
    estadosCivilesDbb = EstadoCivil.objects.all()
    return render(request, 'estadosCiviles.html', {
        "estadosCiviles": list(enumerate(estadosCivilesDbb))
    })

def crearEstadoCivil(request):
    nombre = request.POST['nombre_xg']
    descripcion = request.POST['descripcion_xg']

    newEstadoCivil = EstadoCivil.objects.create(nombre_xg=nombre, descripcion_xg=descripcion)
    messages.success(request, 'Estado civil creado correctamente')

    return redirect('/estadosCiviles')

def eliminarEstadoCivil(request, id):
    estadoCivilDb = EstadoCivil.objects.get(id_xg=id)
    estadoCivilDb.delete()
    messages.success(request, 'Estado civil eliminado correctamente')
    return redirect('/estadosCiviles')

def editarEstadoCivilView(request, id):
    estadoCivilDb = EstadoCivil.objects.get(id_xg=id)
    return render(request, 'editarEstadoCivil.html', {'estadoCivil': estadoCivilDb})

def actualizarEstadoCivil(request):
    id=request.POST['id_xg']
    nombre = request.POST['nombre_xg']
    descripcion = request.POST['descripcion_xg']

    estadoCivilEditar = EstadoCivil.objects.get(id_xg=id)
    estadoCivilEditar.nombre_xg = nombre
    estadoCivilEditar.descripcion_xg = descripcion

    estadoCivilEditar.save()
    messages.success(request, 'Estado civil actualizado exitosamente')
    return redirect('/estadosCiviles')

#TODO: TipoAtencion
def listadoTiposAtencion(request):
    tiposAtencionDbb = TipoAtencion.objects.all()
    return render(request, 'tiposAtencion.html', {
        "tiposAtencion": list(enumerate(tiposAtencionDbb))
    })

def crearTipoAtencion(request):
    nombre = request.POST['nombre_xg']
    descripcion = request.POST['descripcion_xg']
    duracion = request.POST['duracion_xg']
    costo = request.POST['costo_xg']
    fecha_creacion = request.POST['fecha_creacion_xg']

    newTipoAtencion = TipoAtencion.objects.create(nombre_xg=nombre, descripcion_xg=descripcion, duracion_xg=duracion, costo_xg=costo, fecha_creacion_xg=fecha_creacion)
    messages.success(request, 'Tipo de atencion creado correctamente')

    return redirect('/tiposAtencion')

def eliminarTipoAtencion(request, id):
    tipoAtencionDb = TipoAtencion.objects.get(id_xg=id)
    tipoAtencionDb.delete()
    messages.success(request, 'Tipo de atencion eliminado correctamente')
    return redirect('/tiposAtencion')

def editarTipoAtencionView(request, id):
    tipoAtencionDb = TipoAtencion.objects.get(id_xg=id)
    return render(request, 'editarTipoAtencion.html', {'tipoAtencion': tipoAtencionDb})

def actualizarTipoAtencion(request):
    id=request.POST['id_xg']
    nombre = request.POST['nombre_xg']
    descripcion = request.POST['descripcion_xg']
    duracion = request.POST['duracion_xg']
    costo = request.POST['costo_xg']
    fecha_creacion = request.POST['fecha_creacion_xg']

    tipoAtencionEditar = TipoAtencion.objects.get(id_xg=id)
    tipoAtencionEditar.nombre_xg = nombre
    tipoAtencionEditar.descripcion_xg = descripcion
    tipoAtencionEditar.duracion_xg = duracion
    tipoAtencionEditar.costo_xg = costo
    tipoAtencionEditar.fecha_creacion_xg = fecha_creacion

    tipoAtencionEditar.save()
    messages.success(request, 'Tipo de atencion actualizado exitosamente')
    return redirect('/tiposAtencion')

#TODO: Medicamento
def listadoMedicamentos(request):
    medicamentosDbb = Medicamento.objects.all()
    return render(request, 'medicamentos.html', {
        "medicamentos": list(enumerate(medicamentosDbb))
    })

def crearMedicamento(request):
    nombre = request.POST['nombre_xg']
    fecha_vencimiento = request.POST['fecha_vencimiento_xg']
    stock = request.POST['stock_xg']
    costo = request.POST['costo_xg']
    proveedor = request.POST['proveedor_xg']
    fotografia = request.FILES.get('fotografia_xg')

    newMedicamento = Medicamento.objects.create(nombre_xg=nombre, fecha_vencimiento_xg=fecha_vencimiento, stock_xg=stock, costo_xg=costo, proveedor_xg=proveedor, fotografia_xg=fotografia)
    messages.success(request, 'Medicamento creado correctamente')

    return redirect('/medicamentos')

def eliminarMedicamento(request, id):
    medicamentoDb = Medicamento.objects.get(id_xg=id)
    medicamentoDb.delete()
    messages.success(request, 'Medicamento eliminado correctamente')
    return redirect('/medicamentos')

def editarMedicamentoView(request, id):
    medicamentoDb = Medicamento.objects.get(id_xg=id)
    return render(request, 'editarMedicamento.html', {'medicamento': medicamentoDb})

def actualizarMedicamento(request):
    id=request.POST['id_xg']
    nombre = request.POST['nombre_xg']
    fecha_vencimiento = request.POST['fecha_vencimiento_xg']
    stock = request.POST['stock_xg']
    costo = request.POST['costo_xg']
    proveedor = request.POST['proveedor_xg']

    medicamentoEditar = Medicamento.objects.get(id_xg=id)
    medicamentoEditar.nombre_xg = nombre
    medicamentoEditar.fecha_vencimiento_xg = fecha_vencimiento
    medicamentoEditar.stock_xg = stock
    medicamentoEditar.costo_xg = costo
    medicamentoEditar.proveedor_xg = proveedor

    medicamentoEditar.save()
    messages.success(request, 'Medicamento actualizado exitosamente')
    return redirect('/medicamentos')

#TODO: Propietarios
def listadoPropietarios(request):
    propietariosDbb = Propietario.objects.all()
    paisesDbb = Pais.objects.all()
    estadosCivilDbb = EstadoCivil.objects.all()
    return render(request, 'propietarios.html', {
        "propietarios": list(enumerate(propietariosDbb)),
        "paises": paisesDbb,
        "estadosCivil": estadosCivilDbb
    })

def crearPropietario(request):
    nombres = request.POST['nombres_xg']
    apellidos = request.POST['apellidos_xg']
    direccion = request.POST['direccion_xg']
    telefono = request.POST['telefono_xg']
    pais = request.POST['pais_xg']
    paisSeleccionado = Pais.objects.get(id_xg=pais)
    estado_civil = request.POST['estado_civil_xg']
    estadoCivilSeleccionado = EstadoCivil.objects.get(id_xg=estado_civil)
    copia_cedula = request.FILES.get('copia_cedula_xg')

    newPropietario = Propietario.objects.create(nombres_xg=nombres, apellidos_xg=apellidos, direccion_xg=direccion, telefono_xg=telefono, pais_xg=paisSeleccionado, estado_civil_xg=estadoCivilSeleccionado, copia_cedula_xg=copia_cedula)
    messages.success(request, 'Propietario creado correctamente')

    return redirect('/propietarios')

def eliminarPropietario(request, id):
    propietarioDb = Propietario.objects.get(id_xg=id)
    propietarioDb.delete()
    messages.success(request, 'Propietario eliminado correctamente')
    return redirect('/propietarios')

def editarPropietarioView(request, id):
    propietarioDb = Propietario.objects.get(id_xg=id)
    paisesDbb = Pais.objects.all()
    estadosCivilDbb = EstadoCivil.objects.all()
    return render(request, 'editarPropietario.html', {'propietario': propietarioDb, "paises": paisesDbb, "estadosCivil": estadosCivilDbb})

def actualizarPropietario(request):
    id=request.POST['id_xg']
    nombres = request.POST['nombres_xg']
    apellidos = request.POST['apellidos_xg']
    direccion = request.POST['direccion_xg']
    telefono = request.POST['telefono_xg']
    pais = request.POST['pais_xg']
    paisSeleccionado = Pais.objects.get(id_xg=pais)
    estado_civil = request.POST['estado_civil_xg']
    estadoCivilSeleccionado = EstadoCivil.objects.get(id_xg=estado_civil)

    propietarioEditar = Propietario.objects.get(id_xg=id)
    propietarioEditar.nombres_xg = nombres
    propietarioEditar.apellidos_xg = apellidos
    propietarioEditar.direccion_xg = direccion
    propietarioEditar.telefono_xg = telefono
    propietarioEditar.pais_xg = paisSeleccionado
    propietarioEditar.estado_civil_xg = estadoCivilSeleccionado

    propietarioEditar.save()
    messages.success(request, 'Propietario actualizado exitosamente')
    return redirect('/propietarios')

#TODO: Mascotas
def listadoMascotas(request):
    mascotasDbb = Mascota.objects.all()
    propietariosDbb = Propietario.objects.all()
    razasDbb = Raza.objects.all()

    return render(request, 'mascotas.html', {
        'mascotas': list(enumerate(mascotasDbb)),
        'propietarios': propietariosDbb,
        'razas': razasDbb
    })

def crearMascota(request):
    nombre = request.POST['nombre_xg']
    color = request.POST['color_xg']
    peso = request.POST['peso_xg']
    fecha_nacimiento = request.POST['fecha_nacimiento_xg']
    propietario = request.POST['propietario_xg']
    propietarioSeleccionado = Propietario.objects.get(id_xg=propietario)
    raza = request.POST['raza_xg']
    razaSeleccionada = Raza.objects.get(id_xg=raza)
    fotografia = request.FILES.get('fotografia_xg')

    newMascota = Mascota.objects.create(
        nombre_xg=nombre,color_xg=color,peso_xg=peso,fecha_nacimiento_xg=fecha_nacimiento,
        propietario_xg=propietarioSeleccionado,
        raza_xg=razaSeleccionada, fotografia_xg=fotografia
    )
    messages.success(request, 'Mascota creada correctamente')
    return redirect('/mascotas')

def eliminarMascota(request, id):
    mascotaDb = Mascota.objects.get(id_xg=id)
    mascotaDb.delete()

    messages.success(request, 'Mascota eliminada correctamente')
    return redirect('/mascotas')

def editarMascotaView(request, id):
    mascotaDb = Mascota.objects.get(id_xg=id)
    propietariosDbb = Propietario.objects.all()
    razasDbb = Raza.objects.all()

    return render(request, 'editarMascota.html', {
        'mascota': mascotaDb,
        'propietarios': propietariosDbb,
        'razas': razasDbb
    })

def actualizarMascota(request):
    id = request.POST['id_xg']
    nombre = request.POST['nombre_xg']
    color = request.POST['color_xg']
    peso = request.POST['peso_xg']
    fecha_nacimiento = request.POST['fecha_nacimiento_xg']
    propietario = request.POST['propietario_xg']
    propietarioSeleccionado = Propietario.objects.get(id_xg=propietario)
    raza = request.POST['raza_xg']
    razaSeleccionada = Raza.objects.get(id_xg=raza)

    mascotaEditar = Mascota.objects.get(id_xg=id)
    mascotaEditar.nombre_xg = nombre
    mascotaEditar.color_xg = color
    mascotaEditar.peso_xg = peso
    mascotaEditar.fecha_nacimiento_xg = fecha_nacimiento
    mascotaEditar.propietario_xg = propietarioSeleccionado
    mascotaEditar.raza_xg = razaSeleccionada

    mascotaEditar.save()
    messages.success(request, 'Mascota actualizada exitosamente')
    return redirect('/mascotas')

#TODO: Atencion
def listadoAtenciones(request):
    listadoAtencionesDbb = Atencion.objects.all()
    mascotasDbb = Mascota.objects.all()
    tiposAtencionDbb = TipoAtencion.objects.all()
    return render(request, 'atenciones.html', {
        'atenciones': list(enumerate(listadoAtencionesDbb)),
        'mascotas': mascotasDbb,
        'tiposAtencion': tiposAtencionDbb
    })

def crearAtencion(request):
    fecha = request.POST['fecha_xg']
    descripcion = request.POST['descripcion_xg']
    seguimiento = request.POST['seguimiento_xg']
    tipo_atencion = request.POST['tipo_atencion_xg']
    tipoAtencionSeleccionado = TipoAtencion.objects.get(id_xg=tipo_atencion)
    mascota = request.POST['mascota_xg']
    mascotaSeleccionada = Mascota.objects.get(id_xg=mascota)

    newAtencion = Atencion.objects.create(
        fecha_xg=fecha,
        descripcion_xg=descripcion,
        seguimiento_xg=seguimiento,
        tipo_atencion_xg=tipoAtencionSeleccionado,
        mascota_xg=mascotaSeleccionada
    )
    messages.success(request, 'Atencion creada correctamente')
    return redirect('/atenciones')

def eliminarAtencion(request, id):
    atencionDb = Atencion.objects.get(id_xg=id)
    atencionDb.delete()

    messages.success(request, 'Atencion eliminada correctamente')
    return redirect('/atenciones')

def editarAtencionView(request, id):
    atencionDb = Atencion.objects.get(id_xg=id)
    mascotasDbb = Mascota.objects.all()
    tiposAtencionDbb = TipoAtencion.objects.all()

    return render(request, 'editarAtencion.html', {
        'atencion': atencionDb,
        'mascotas': mascotasDbb,
        'tiposAtencion': tiposAtencionDbb
    })

def actualizarAtencion(request):
    id = request.POST['id_xg']
    fecha = request.POST['fecha_xg']
    descripcion = request.POST['descripcion_xg']
    seguimiento = request.POST['seguimiento_xg']
    tipo_atencion = request.POST['tipo_atencion_xg']
    tipoAtencionSeleccionado = TipoAtencion.objects.get(id_xg=tipo_atencion)
    mascota = request.POST['mascota_xg']
    mascotaSeleccionada = Mascota.objects.get(id_xg=mascota)

    atencionEditar = Atencion.objects.get(id_xg=id)
    atencionEditar.fecha_xg = fecha
    atencionEditar.descripcion_xg = descripcion
    atencionEditar.seguimiento_xg = seguimiento
    atencionEditar.tipo_atencion_xg = tipoAtencionSeleccionado
    atencionEditar.mascota_xg = mascotaSeleccionada

    atencionEditar.save()
    messages.success(request, 'Atencion actualizada exitosamente')
    return redirect('/atenciones')

#todo: recetas
def listadoRecetas(request):
    recetasDbb = Receta.objects.all()
    atencionesDbb = Atencion.objects.all()

    return render(request, 'recetas.html', {
        'recetas': list(enumerate(recetasDbb)),
        'atenciones': atencionesDbb
    })

def crearReceta(request):
    instrucciones = request.POST['instrucciones_xg']
    notas_adicionales = request.POST['notas_adicionales_xg']
    fecha = request.POST['fecha_xg']
    atencion = request.POST['atencion_xg']
    atencionSeleccionada = Atencion.objects.get(id_xg=atencion)

    newReceta = Receta.objects.create(instrucciones_xg=instrucciones, notas_adicionales_xg=notas_adicionales,fecha_xg=fecha, atencion_xg=atencionSeleccionada)
    messages.success(request, 'Receta creado correctamente')

    return redirect('/recetas')

def eliminarReceta(request, id):
    recetaDB = Receta.objects.get(id_xg=id)
    recetaDB.delete()
    messages.success(request, 'Receta eliminado correctamente')

    return redirect('/recetas')

def editarRecetaView(request, id):
    recetaDb = Receta.objects.get(id_xg=id)
    atencionesDbb = Atencion.objects.all()

    return render(request, 'editarReceta.html', {
        'receta': recetaDb,
        'atenciones': atencionesDbb
    })

def actualizarReceta(request):
    id = request.POST['id_xg']
    fecha = request.POST['fecha_xg']
    instrucciones = request.POST['instrucciones_xg']
    notas_adicionales = request.POST['notas_adicionales_xg']
    atencion = request.POST['atencion_xg']
    atencionSeleccionada = Atencion.objects.get(id_xg=atencion)

    recetaEditar = Receta.objects.get(id_xg=id)
    recetaEditar.fecha_xg = fecha
    recetaEditar.instrucciones_xg = instrucciones
    recetaEditar.notas_adicionales_xg = notas_adicionales
    recetaEditar.atencion_xg = atencionSeleccionada

    recetaEditar.save()
    messages.success(request, 'Receta actualizada correctamente')
    return redirect('/recetas')

#TODO: detalle
def listadoDetalles(request):
    detallesDbb = Detalle.objects.all()
    medicamentosDbb = Medicamento.objects.all()
    recetasDbb = Receta.objects.all()

    return render(request, 'detalles.html', {
        'detalles': list(enumerate(detallesDbb)),
        'medicamentos': medicamentosDbb,
        'recetas':recetasDbb
    })

def crearDetalle(request):
    cantidad = request.POST['cantidad_xg']
    dosis = request.POST['dosis_xg']
    frecuencia = request.POST['frecuencia_xg']
    medicamento = request.POST['medicamento_xg']
    receta = request.POST['receta_xg']
    medicamentoSeleccionado = Medicamento.objects.get(id_xg=medicamento)
    recetaSeleccionado = Receta.objects.get(id_xg=receta)

    newDetalle = Detalle.objects.create(cantidad_xg=cantidad, dosis_xg=dosis, frecuencia_xg=frecuencia, medicamento_xg=medicamentoSeleccionado, receta_xg=recetaSeleccionado)

    messages.success(request, 'Detalle creado correctamente')
    return redirect('/detalles')

def eliminarDetalle(request, id):
    detalleDb = Detalle.objects.get(id_xg=id)
    detalleDb.delete()

    messages.success(request, 'Detalle eliminado correctamente')
    return redirect('/detalles')

def editarDetalleView(request, id):
    detalleDb = Detalle.objects.get(id_xg=id)
    medicamentosDb = Medicamento.objects.all()
    recetasDb = Receta.objects.all()

    return render(request, 'editarDetalle.html', {
        'detalle': detalleDb,
        'medicamentos': medicamentosDb,
        'recetas': recetasDb
    })

def actualizarDetalle(request):
    id = request.POST['id_xg']
    cantidad = request.POST['cantidad_xg']
    dosis = request.POST['dosis_xg']
    frecuencia = request.POST['frecuencia_xg']
    medicamento = request.POST['medicamento_xg']
    receta = request.POST['receta_xg']

    medicamentoSeleccionado = Medicamento.objects.get(id_xg=medicamento)
    recetaSeleccionado = Receta.objects.get(id_xg=receta)

    detalleEditar = Detalle.objects.get(id_xg=id)
    detalleEditar.cantidad_xg = cantidad
    detalleEditar.dosis_xg = dosis
    detalleEditar.frecuencia_xg = frecuencia
    detalleEditar.medicamento_xg = medicamentoSeleccionado
    detalleEditar.receta_xg = recetaSeleccionado
    detalleEditar.save()

    messages.success(request, 'Detalle actualizado correctamente')
    return redirect('/detalles')